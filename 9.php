<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>กรมอนามัย
    </title>

    <?php include 'assets/include/inc-head.php'; ?>
</head>

<body class="sidebar-double-icon  ">
    <div id="loading">
        <div id="loading-center">
        </div>
    </div>

    <div class="wrapper">
        <section class="login-content">
            <?php include 'assets/include/inc-menuleft.php'; ?>
            <?php include 'assets/include/inc-header.php'; ?>
        <div class="content-page">
            <div class="container-fluid">


                <div class="row align-items-center justify-content-center mt-4">
                    <div class="col-sm-12 col-lg-9">
                        <div class="card">

                            <div class="card-header d-flex justify-content-between">
                                <div class="header-title">
                                    <h4 class="card-title">ลงทะเบียนบัตรเจ้าหน้าที่ท้องถิ่น / เจ้าพนักงานสาธารณสุข/ <br> 
                                    ผู้ซึ่งได้รับการแต่งตั้งจากเจ้าพนักงานท้องถิ่น</h4>
                                </div>
                            </div>

                            <div class="card-body">
                            <h5 class="mb-3"><strong>ข้อมูลส่วนตัว</strong></h5>
                                <div class="form-row">
                                    <label class="col-lg-6">
                                    <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>เลขประจำตัวประชาชน</label>
                                        </div>
                                    </label>
                                </div>
                                <div class="form-row">
                                    <label class="col-lg-2">
                                        <select class="form-control">
                                            <option selected="">คำนำหน้า</option>
                                            <option value="1">นาย</option>
                                            <option value="2">นาง</option>
                                            <option value="3">นางสาว</option>
                                        </select>
                                    </label>
                                    <label class="col-lg-5">
                                    <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>ชื่อ</label>
                                        </div>
                                    </label>
                                    <label class="col-lg-5">
                                    <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>นามสกุล</label>
                                        </div>
                                    </label>
                                </div>
                                <div class="form-row">
                                    <label class="col-lg-4">
                                        <select class="form-control">
                                            <option selected="">เพศ</option>
                                            <option value="1">ชาย</option>
                                            <option value="2">หญิง</option>
                                        </select>
                                    </label>

                                    <label class="col-lg-4">
                                        <select class="form-control">
                                            <option selected="">หมู่เลือด</option>
                                            <option value="1">AB</option>
                                            <option value="2">A</option>
                                            <option value="3">B</option>
                                            <option value="4">O</option>
                                        </select>
                                    </label>

                                    <label class="col-lg-4">
                                    <input placeholder="วัน/เดือน/ปีเกิด" onfocus="(this.type='date')"  type="text" class="form-control">
                                    </label>
                                </div>

                                <div class="form-row">
                                    <label class="col-lg-6">
                                    <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>เบอร์โทรศัพท์</label>
                                        </div>
                                    </label>
                                    <label class="col-lg-6">
                                    <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>อีเมล</label>
                                        </div>
                                    </label>
                                    
                                </div>


                                <hr>

                                <h5 class=" mt-2 mb-3"><strong>ที่อยู่ปัจจุบัน</strong></h5>
                                <div class="form-row">
                                    <label class="col-lg-3">
                                    <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>บ้านเลขที่</label>
                                        </div>
                                    </label>
                                    <label class="col-lg-3">
                                    <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>หมู่</label>
                                        </div>
                                    </label>
                                    <label class="col-lg-6">
                                    <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>ถนน</label>
                                        </div>
                                    </label>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-3">
                                        <select class="form-control mb-3">
                                            <option selected="">จังหวัด</option>
                                            <option value="1">#</option>
                                            <option value="2">#</option>
                                            <option value="3">#</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control mb-3">
                                            <option selected="">อำเภอ</option>
                                            <option value="1">#</option>
                                            <option value="2">#</option>
                                            <option value="3">#</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3">
                                        <select class="form-control mb-3">
                                            <option selected="">ตำบล</option>
                                            <option value="1">#</option>
                                            <option value="2">#</option>
                                            <option value="3">#</option>
                                        </select>
                                    </div>

                                    <label class="col-lg-3">
                                    <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>รหัสไปรษณีย์</label>
                                        </div>
                                    </label>

                                </div>
                                
                                <hr>
                               
                               <div class="form-row">
                                    <label class="col-lg-6">
                                        <select class="form-control">
                                            <option selected="">วุฒิการศึกษา</option>
                                            <option value="1">#</option>
                                            <option value="2">#</option>
                                            <option value="3">#</option>
                                            <option value="4">#</option>
                                            <option value="5">#</option>
                                        </select>
                                    </label>
                                    <label class="col-lg-6">
                                    <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>สาขา</label>
                                        </div>
                                    </label>
                               </div>

                               <div class="form-row">
                                    <label class="col-lg-6">
                                        <div class="input-group mb-4">
                                            <input type="text" class="form-control" placeholder="ประวัติการทำงาน (อายุงาน)" aria-label="ประวัติการทำงาน (อายุงาน)" aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">ปี</span>
                                            </div>
                                        </div>
                                    </label>

                                    <label class="col-lg-3">
                                        <div class="input-group mb-4">
                                            <input placeholder="เริ่มงาน" onfocus="(this.type='date')"  type="text" class="form-control">
                                        </div>
                                    </label>

                                    <label class="col-lg-3">
                                        <div class="input-group mb-4">
                                            <input placeholder="ปัจจุบัน" onfocus="(this.type='date')"  type="text" class="form-control">
                                        </div>
                                    </label>
                               </div>

                               <h5 class=" mt-2 mb-2"><strong>ข้อมูลหน่วยงาน</strong></h5>
                               <div class="form-row">
                                    <label class="col-lg-6">
                                    <select class="form-control">
                                            <option selected="">ประเภทบุคคล</option>
                                            <option value="1">#</option>
                                            <option value="2">#</option>
                                            <option value="3">#</option>
                                            <option value="4">#</option>
                                            <option value="5">#</option>
                                        </select>
                                    </label>
                               </div>

                               <div class="form-row">
                                    <label class="col-lg-6">
                                    <select class="form-control">
                                            <option selected="">ตำแหน่ง</option>
                                            <option value="1">#</option>
                                            <option value="2">#</option>
                                            <option value="3">#</option>
                                            <option value="4">#</option>
                                            <option value="5">#</option>
                                        </select>
                                    </label>
                                    <label class="col-lg-6">
                                    <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>ระดับ</label>
                                        </div>
                                    </label>

                               </div>

                               <div class="form-row">
                                    <label class="col-lg-6">
                                    <select class="form-control">
                                            <option selected="">ประเภทเจ้าพนักงาน</option>
                                            <option value="1">#</option>
                                            <option value="2">#</option>
                                            <option value="3">#</option>
                                            <option value="4">#</option>
                                            <option value="5">#</option>
                                        </select>
                                    </label>
                                    <label class="col-lg-6">
                                    <select class="form-control">
                                            <option selected="">สังกัดหน่วยงาน</option>
                                            <option value="1">#</option>
                                            <option value="2">#</option>
                                            <option value="3">#</option>
                                            <option value="4">#</option>
                                            <option value="5">#</option>
                                        </select>
                                    </label>

                               </div>

                               <hr>
                                 
                               
                             
                                <div class="form-row">
                                   <div class="col-lg-12">
                                   <label class="mt-2" > <strong> อัปโหลดเอกสาร </strong>
                                   <span class="text-muted" style="font-size: 12px">
                                          (คำสั่ง,ผลการฝึกอบรม)
                                   </span> </label>
                                   
                                   </div>

                                    <label class="col-lg-6">
                                        <input type="file" class="custom-file-input" id="customFile">
                                        <label class="custom-file-label" for="customFile">เลือกไฟล์</label>
                                    </label>
                                    <label class="col-lg-6">
                                    </label>
                                </div>
                            </div>
                        </div>

                      
                        <div class="row align-items-center justify-content-center">

                            <button type="button" class="btn btn-primary mt-2">ยืนยันลงทะเบียน</button>

                        </div>
                        <div class="row">
                            <div class="col-md-12 mb-4"> <button type="button" class="mt-2 btn btn-outline-primary">ย้อนกลับ</button></div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        </section>
    </div>

    <?php include 'assets/include/inc-script.php' ?>
</body>

</html>