<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>กรมอนามัย
    </title>

    <?php include 'assets/include/inc-head.php'; ?>
</head>

<body class="sidebar-double-icon  ">
    <div id="loading">
        <div id="loading-center">
        </div>
    </div>

    <div class="wrapper">
        <?php include 'assets/include/inc-menuleft.php'; ?>
        <?php include 'assets/include/inc-header.php'; ?>
        <div class="content-page">
            <div class="container-fluid">
                <div class="col-md-12 mb-2">
                    <h4><b>ลงทะเบียนขอบัตรประจำตัว</b></h4>
                </div>

                <div class="row mt-4">


                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="card card-block card-stretch card-height">
                            <div class="card-body">
                                <img src="assets/images/1-main/Frame.png" class="card-img-top" alt="#">
                                <div class="mt-4">

                                    <h5 class="text-center">บัตรประจำตัวผู้ประกอบกิจการและผู้สัมผัสอาหาร<h5><br>
                                            <div class="row align-items-center justify-content-center">

                                                <button type="button" class="btn btn-primary mt-4 px-3">ลงทะเบียน</button>
                                            </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="card card-block card-stretch card-height">
                            <div class="card-body">
                                <img src="assets/images/1-main/Frame.png" class="card-img-top" alt="#">
                                <div class="mt-4">

                                    <h5 class="text-center">เจ้าหน้าที่ท้องถิ่น / เจ้าพนักงานสาธารณสุข/ผู้ซึ่งได้รับการแต่งตั้งจากเจ้าพนักงานท้องถิ่น<h5>
                                            <div class="row align-items-center justify-content-center">

                                                <button type="button" class="btn btn-primary mt-4">ลงทะเบียน</button>
                                            </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="card card-block card-stretch card-height">
                            <div class="card-body">
                                <img src="assets/images/1-main/Frame.png" class="card-img-top" alt="#">
                                <div class="mt-4">

                                    <h5 class="text-center">บัตรพนักงานเจ้าหน้าที่ตาม พรบ.ควบคุมการส่งเสริมการตลาดอาหารสำหรับทารกและเด็กเล็ก<h5>
                                    
                                            <div class="row align-items-center justify-content-center">

                                                <button type="button" class="btn btn-primary mt-4">ลงทะเบียน</button>
                                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="card card-block card-stretch card-height">
                            <div class="card-body">
                                <img src="assets/images/1-main/Frame.png" class="card-img-top" alt="#">
                                <div class="mt-4">

                                    <h5 class="text-center">บัตรประจำตัวผู้จัดการดูแลผู้สูงอายุ และ ผู้ดูแลผู้สูงอายุ<h5>
                                    
                                            <div class="row align-items-center justify-content-center">

                                                <button type="button" class="btn btn-primary mt-4">ลงทะเบียน</button>
                                            </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12 mt-3">
                    <h4><b>ลงทะเบียนอื่นๆ</b></h4>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <div class="card card-block card-stretch card-height">
                            <div class="card-body">
                                <img src="assets/images/1-main/Frame.png" class="card-img-top" alt="#">
                                <div class="mt-4">

                                    <h5 class="text-center">บัตร xxxxxxxxx<h5>
                                    
                                            <div class="row align-items-center justify-content-center">

                                                <button type="button" class="btn btn-primary mt-4 px-3">ลงทะเบียน</button>
                                            </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>
    </div>

    <?php include 'assets/include/inc-script.php' ?>
</body>

</html>