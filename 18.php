<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>กรมอนามัย
    </title>

    <?php include 'assets/include/inc-head.php'; ?>
</head>

<body class="sidebar-double-icon  ">
    <div id="loading">
        <div id="loading-center">
        </div>
    </div>




    <div class="wrapper">
        <section class="login-content">
            <?php include 'assets/include/inc-menuleft.php'; ?>
            <?php include 'assets/include/inc-header.php'; ?>
            <div class="content-page">
                <div class="container-fluid">
                    <div class="row align-items-center justify-content-center mt-4">
                        <div class="col-sm-12 col-lg-9">
                            <div class="card">

                                <div class="card-header d-flex justify-content-between">
                                    <div class="header-title">
                                        <h4 class="card-title">บัตร XXXXXXXXXXXXXX</h4>
                                    </div>
                                </div>

                                <div class="card-body">

                                    <div class="form-row">
                                        <label class="col-lg-2">
                                            <select class="form-control">
                                                <option selected="">คำนำหน้า</option>
                                                <option value="1">นาย</option>
                                                <option value="2">นาง</option>
                                                <option value="3">นางสาว</option>
                                            </select>
                                        </label>

                                        <label class="col-lg-4">
                                            <div class="floating-label form-group">
                                                <input class="floating-input form-control" type="text" placeholder=" ">
                                                <label>ชื่อ</label>
                                            </div>
                                        </label>


                                        <label class="col-lg-4">
                                            <div class="floating-label form-group">
                                                <input class="floating-input form-control" type="text" placeholder=" ">
                                                <label>นามสกุล</label>
                                            </div>
                                        </label>
                                        <label class="col-lg-2">
                                            <select class="form-control">
                                                <option selected="">หมู่เลือด</option>
                                                <option value="1">AB</option>
                                                <option value="2">A</option>
                                                <option value="3">B</option>
                                                <option value="4">O</option>
                                            </select>
                                        </label>
                                    </div>

                                    <div class="form-row">
                                        <label class="col-lg-6">
                                            <div class="floating-label form-group">
                                                <input class="floating-input form-control" type="text" placeholder=" ">
                                                <label>เลขบัตรประชาชน</label>
                                            </div>

                                        </label>
                                        <label class="col-lg-6">
                                            <input placeholder="วัน/เดือน/ปีเกิด" onfocus="(this.type='date')" type="text" class="form-control">
                                        </label>
                                    </div>


                                    <h5 class="mb-2"><label><strong>ที่อยู่ปัจจุบัน</strong></label></h5>
                                    <div class="form-row">
                                        <label class="col-lg-3">
                                            <div class="floating-label form-group">
                                                <input class="floating-input form-control" type="text" placeholder=" ">
                                                <label>บ้านเลขที่</label>
                                            </div>

                                        </label>
                                        <label class="col-lg-3">
                                            <div class="floating-label form-group">
                                                <input class="floating-input form-control" type="text" placeholder=" ">
                                                <label>หมู่</label>
                                            </div>

                                        </label>
                                        <label class="col-lg-6">
                                            <div class="floating-label form-group">
                                                <input class="floating-input form-control" type="text" placeholder=" ">
                                                <label>ถนน</label>
                                            </div>

                                        </label>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-lg-3">
                                            <select class="form-control mb-3">
                                                <option selected="">จังหวัด</option>
                                                <option value="1">#</option>
                                                <option value="2">#</option>
                                                <option value="3">#</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3">
                                            <select class="form-control mb-3">
                                                <option selected="">อำเภอ</option>
                                                <option value="1">#</option>
                                                <option value="2">#</option>
                                                <option value="3">#</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-3">
                                            <select class="form-control mb-3">
                                                <option selected="">ตำบล</option>
                                                <option value="1">#</option>
                                                <option value="2">#</option>
                                                <option value="3">#</option>
                                            </select>
                                        </div>

                                        <label class="col-lg-3">
                                            <div class="floating-label form-group">
                                                <input class="floating-input form-control" type="text" placeholder=" ">
                                                <label>รหัสไปรษณีย์</label>
                                            </div>

                                        </label>

                                    </div>

                                    <div class="form-row">
                                        <label class="col-lg-5">
                                            <div class="floating-label form-group">
                                                <input class="floating-input form-control" type="text" placeholder=" ">
                                                <label>เบอร์โทรศัพท์</label>
                                            </div>

                                        </label>
                                    </div>
                                    <div class="form-row">
                                        <label class="col-lg-6">
                                            <div class="floating-label form-group">
                                                <input class="floating-input form-control" type="text" placeholder=" ">
                                                <label>อีเมล์</label>
                                            </div>

                                        </label>
                                        <label class="col-lg-6">
                                            <div class="floating-label form-group">
                                                <input class="floating-input form-control" type="text" placeholder=" ">
                                                <label>ยืนยันอีเมล์</label>
                                            </div>

                                        </label>
                                    </div>
                                    <hr>
                                    <div class="form-row">
                                        <label class="col-lg-6">
                                            <select class="form-control mb-3">
                                                <option selected="">วุฒิการศึกษา</option>
                                                <option value="1">#</option>
                                                <option value="2">#</option>
                                                <option value="3">#</option>
                                            </select>
                                        </label>
                                        <label class="col-lg-6">
                                            <div class="floating-label form-group">
                                                <input class="floating-input form-control" type="text" placeholder=" ">
                                                <label>สาขา</label>
                                            </div>

                                        </label>
                                    </div>
                                    <div class="form-row">
                                        <label class="col-lg-6">
                                            <div class="input-group mb-4">
                                                <input type="text" class="form-control" placeholder="ประวัติการทำงาน (อายุงาน)" aria-label="ประวัติการทำงาน (อายุงาน)" aria-describedby="basic-addon2">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">ปี</span>
                                                </div>
                                            </div>
                                        </label>
                                        <label class="col-lg-3">
                                            <input placeholder="เริ่มงาน" onfocus="(this.type='date')" type="text" class="form-control">
                                        </label>
                                        <label class="col-lg-3">
                                            <input placeholder="ปัจจุบัน" onfocus="(this.type='date')" type="text" class="form-control">
                                        </label>
                                    </div>
                                    <div class="form-row">
                                        <label class="col-lg-6">
                                            <select class="form-control mb-3">
                                                <option selected="">ประเภทบุคคล</option>
                                                <option value="1">#</option>
                                                <option value="2">#</option>
                                                <option value="3">#</option>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="form-row">
                                        <label class="col-lg-6">
                                            <select class="form-control mb-3">
                                                <option selected="">ตำแหน่ง</option>
                                                <option value="1">#</option>
                                                <option value="2">#</option>
                                                <option value="3">#</option>
                                            </select>
                                        </label>
                                        <label class="col-lg-6">
                                            <div class="floating-label form-group">
                                                <input class="floating-input form-control" type="text" placeholder=" ">
                                                <label>ระดับ</label>
                                            </div>

                                        </label>
                                    </div>
                                    <div class="form-row">
                                        <label class="col-lg-6">
                                            <select class="form-control mb-3">
                                                <option selected="">ประเภทเจ้าพนักงาน</option>
                                                <option value="1">#</option>
                                                <option value="2">#</option>
                                                <option value="3">#</option>
                                            </select>
                                        </label>
                                        <label class="col-lg-6">
                                            <select class="form-control mb-3">
                                                <option selected="">สังกัดหน่วยงาน</option>
                                                <option value="1">#</option>
                                                <option value="2">#</option>
                                                <option value="3">#</option>
                                            </select>
                                        </label>
                                    </div>
                                    <div class="form-row">
                                        <label class="col-lg-1"></label>
                                        <label class="col-lg-3">
                                            <h5 class="mb-1"><label><strong>อัพโหลดรูปติดบัตร</strong></label></h5>

                                        </label>
                                    </div>

                                    <div class="form-row">
                                        <label class="col-lg-1"></label>
                                        <label class="col-lg-2">
                                            <div class="media mr-4 mb-4">
                                                <div class="crm-profile-img-edit">
                                                    <img class="crm-profile-pic rounded-circle avatar-100 ml-2" src="assets/images/1-main/1.jpg" alt="profile-pic">
                                                    <div class="crm-p-image bg-primary">
                                                        <i class="fas fa-pencil-alt mt-2"></i>
                                                        <input type="file" class="file-upload" accept="image/*">
                                                    </div>
                                                </div>
                                            </div>
                                        </label>
                                    </div>

                                    <hr>
                                    <div class="form-row">
                                        <label class="col-lg-2"></label>
                                        <label class="col-lg-5">
                                            <div class="floating-label form-group">
                                                <input class="floating-input form-control" type="text" placeholder=" ">
                                                <label>ชื่อหน่วยงานจัดอบรม</label>
                                            </div>

                                        </label>
                                        <label class="col-lg-3">
                                            <input placeholder="วันที่ผ่านการอบรม" onfocus="(this.type='date')" type="text" class="form-control">
                                        </label>
                                        <label class="col-lg-2"></label>
                                    </div>
                                    <div class="form-row">
                                        <label class="col-lg-2"></label>
                                        <label class="col-lg-10">
                                            <label><strong>อัปโหลดเอกสาร
                                                    <span class="text-muted" style="font-size: 12px">
                                                        (คำสั่ง,ผลการฝึกอบรม)
                                                    </span></strong>
                                            </label>
                                        </label>
                                        <label class="col-lg-2"></label>
                                        <label class="col-lg-5">

                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="inputGroupFile01">
                                                <label class="custom-file-label" for="inputGroupFile01">เลือกไฟล์</label>
                                            </div>
                                        </label>
                                        <label class="col-lg-5"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row align-items-center justify-content-center">
                                <button type="button" class="btn btn-primary mt-2">ยืนยันลงทะเบียน</button>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-4"> <button type="button" class="mt-2 btn btn-outline-primary">ย้อนกลับ</button></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    <?php include 'assets/include/inc-script.php' ?>
</body>



</html>