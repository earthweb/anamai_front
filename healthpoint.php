<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>กรมอนามัย
    </title>

    <?php include 'assets/include/inc-head.php'; ?>
</head>

<body class="sidebar-double-icon intro-page">
    <div id="loading">
        <div id="loading-center">
        </div>
    </div>
    <div class="wrapper">
        <section class="login-content" style="height:auto">
            <img src="assets/images/1-main/bg-building.png" class="intro-img" alt="">
            <div class="mt-1 text-center">
                <img src="assets/images/1-main/logo-intro.png" class="" alt="">
                <br><br>
                <h3 style="color:white">ระบบการขอมีบัตรประจำตัวและ</h3>
                <h3 style="color:white">ประกาศนียบัตรภายใต้ภารกิจของกรมอนามัย</h3>
            </div>

            <div class="container">
                <div class="row align-items-center justify-content-center mt-5">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header justify-content-between">
                                <div class="header-title text-cnter">
                                    <h4 class="card-title ">ตรวจสอบคะแนน Health Point</h4>
                                </div>
                            </div>
                            <div class="card-body">

                                <form>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="floating-label form-group">
                                                <input class="floating-input form-control" type="email" placeholder=" ">
                                                <label>กรอกบัตรประจำตัวประชาชน</label>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">

                                        </div>
                                    </div>
                                    <center>
                                        <button type="submit" class="btn btn-primary btn-lg text-center">ยืนยัน</button>
                                    </center>
                                </form>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header justify-content-between">
                                <div class="header-title text-cnter">
                                    <h4 class="card-title ">คะแนน Health Point</h4>
                                </div>
                            </div>
                            <div class="card-body">

                                <form>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="floating-label form-group">

                                            </div>
                                        </div>

                                        <div class="col-lg-6">

                                        </div>
                                    </div>
                                    <center>
                                        <h4><span class="text-primary">10</span> Point</h4>
                                    </center>
                                   
                                </form>
                            </div>
                        </div>

                    </div>
                   
                </div>
                <div class="row">
                <div class="col-md-12 ml-5 mb-4"> <button type="button" class="mt-2 btn btn-light rounded-pill"><i class="fa fa-angle-left" aria-hidden="true"></i>ย้อนกลับ</button></div>
                </div>
            </div>
        </section>

    </div>
    <?php include 'assets/include/inc-script.php' ?>
</body>

</html>