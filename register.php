<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>กรมอนามัย</title>

    <?php include 'assets/include/inc-head.php'; ?>
</head>

<body class="sidebar-double-icon intro-page">
    <div id="loading">
        <div id="loading-center">
        </div>
    </div>
    <div class="wrapper">
        <section class="login-content">
            <img src="assets/images/1-main/bg-building.png" class="intro-img" alt="">
            <div class="container ">
                <div class="mt-1 text-center">
                    <img src="assets/images/1-main/logo-intro.png" class="" alt="">
                    <br><br>
                    <h3>ลงทะเบียนบัญชีผู้ใช้</h3>
                </div>

                <div class="row align-items-center justify-content-center mt-4">
                    <div class="col-sm-12 col-lg-9">
                        <div class="card">
                            <div class="card-body">
                                <h5><label><strong>ประเภทผู้ใช้งาน</strong></label></h5>
                                <div class="form-row">
                                    <label class="col-lg-6">
                                        <select class="form-control mb-3">
                                            <option selected="">เลือกประเภทผู้ใช้งาน</option>
                                            <option value="1">บุคคลทั่วไป</option>
                                            <option value="2">พนักงานราชการ/ข้าราชการ</option>
                                        </select>
                                    </label>
                                </div>

                                <h5 class="mb-2"><label><strong>ข้อมูลผู้ใช้งาน</strong></label></h5>
                                <div class="form-row">
                                    <label class="col-lg-6">
                                        <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>เลขประจำตัวประชาชน</label>
                                        </div>
                                    </label>
                                </div>
                                <div class="form-row">
                                    <label class="col-lg-2">
                                        <select class="form-control">
                                            <option selected="">คำนำหน้า</option>
                                            <option value="1">นาย</option>
                                            <option value="2">นาง</option>
                                            <option value="3">นางสาว</option>
                                        </select>
                                    </label>
                                    <label class="col-lg-5">
                                        <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>ชื่อ</label>
                                        </div>
                                    </label>
                                    <label class="col-lg-5">
                                        <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>นามสกุล</label>
                                        </div>
                                    </label>
                                </div>
                                <div class="form-row">
                                    <label class="col-lg-3">
                                        <select class="form-control">
                                            <option selected="">เพศ</option>
                                            <option value="1">ชาย</option>
                                            <option value="2">หญิง</option>
                                        </select>
                                    </label>

                                    <label class="col-lg-3">
                                        <select class="form-control">
                                            <option selected="">หมู่เลือด</option>
                                            <option value="1">AB</option>
                                            <option value="2">A</option>
                                            <option value="3">B</option>
                                            <option value="4">O</option>
                                        </select>
                                    </label>

                                    <label class="col-lg-6">
                                        <input type="date" class="form-control">
                                    </label>
                                </div>

                                <div class="form-row">
                                    <label class="col-lg-12">
                                        <select class="form-control">
                                            <option selected="">ประเภทบุคคล</option>
                                            <option value="1">#</option>
                                            <option value="2">#</option>
                                            <option value="3">#</option>
                                        </select>
                                    </label>
                                </div>
                                <div class="form-row">
                                    <label class="col-lg-6">
                                        <select class="form-control">
                                            <option selected="">ตำแหน่ง</option>
                                            <option value="1">#</option>
                                            <option value="2">#</option>
                                            <option value="3">#</option>
                                        </select>
                                    </label>
                                    <label class="col-lg-6">
                                        <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>ระดับ</label>
                                        </div>
                                    </label>
                                </div>
                                <hr>

                                <h5 class="mb-2 mt-2"><label><strong>ที่อยู่ปัจจุบัน</strong></label></h5>
                                <div class="form-row">
                                    <label class="col-lg-3">
                                        <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>บ้านเลขที่</label>
                                        </div>
                                    </label>
                                    <label class="col-lg-3">
                                        <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>หมู่</label>
                                        </div>
                                    </label>
                                    <label class="col-lg-6">
                                        <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>ถนน</label>
                                        </div>
                                    </label>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-3">
                                        <select class="form-control mb-3">
                                            <option selected="">จังหวัด</option>
                                            <option value="1">#</option>
                                            <option value="2">#</option>
                                            <option value="3">#</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control mb-3">
                                            <option selected="">อำเภอ</option>
                                            <option value="1">#</option>
                                            <option value="2">#</option>
                                            <option value="3">#</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3">
                                        <select class="form-control mb-3">
                                            <option selected="">ตำบล</option>
                                            <option value="1">#</option>
                                            <option value="2">#</option>
                                            <option value="3">#</option>
                                        </select>
                                    </div>

                                    <label class="col-lg-3">
                                        <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>รหัสไปรษณีย์</label>
                                        </div>
                                    </label>

                                </div>


                                <h5 class="mt-2"><label>อัปโหลดรูปติดบัตร</label></h5>
                                <div class="form-row">
                                    <label class="col-lg-6">
                                        <input type="file" class="custom-file-input" id="customFile">
                                        <label class="custom-file-label" for="customFile">เลือกรูปภาพ</label>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <h5 class="mb-2"> <label><strong>ข้อมูลสำหรับผู้ใช้</strong></label></h5>
                                <div class="form-row">
                                    <label class="col-lg-6">
                                        <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="text" placeholder=" ">
                                            <label>ชื่อผู้ใช้งาน</label>
                                        </div>
                                    </label>
                                </div>
                                <div class="form-row">
                                    <label class="col-lg-6">
                                        <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="password" placeholder=" ">
                                            <label>รหัสผ่าน</label>
                                        </div>
                                    </label>
                                </div>
                                <div class="form-row">
                                    <label class="col-lg-6">
                                        <div class="floating-label form-group">
                                            <input class="floating-input form-control" type="password" placeholder=" ">
                                            <label>ยืนยันรหัสผ่าน</label>
                                        </div>
                                    </label>
                                </div>


                            </div>
                        </div>

                        <div class="col-md-12 mb-4">
                            <button type="button" class="mt-2 btn btn-outline-primary">ย้อนกลับ</button>
                            <button type="button" class="btn btn-primary mt-2" style="margin-left: 300px;">ยืนยันบัญชี</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php include 'assets/include/inc-script.php' ?>
</body>

</html>